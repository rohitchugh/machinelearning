import pandas as pd
p = pd.read_csv('/PATH TO FILE')	#LOAD THE DATAFRAMES FROM CSV FILE ||for autos.csv ---> pd.read_csv('autos.csv', encoding = 'cp1252')

#--------OR WE USE DATASETS AND CONVERT THEM TO DATAFRAMES-----------
from sklearn import datasets
iris = datasets.load_iris()
type(iris.data) #SHOWS THE TYPE

#CONVERSION OF ARRAY TO DATAFRAME
p = pd.DataFrame(iris.data) #COVERSION
p = pd.DataFrame(iris.data, columns=iris.feature_names) #COLUMNS CAN BE NAMED

#MANIPULATING DATA IN DATAFRAME

p.info() #INFORMATION ABOUT OUR DATAFRAME

P["sepal length (cm)"][0] = None #CHANGING THE FIRST ENTRY TO NONE
p.info() #1 Non Null entry

P["sepal length (cm)"][0] = "abc"
p.info() # "OBJECT" MEANS NOT ALL ENTRIES OF SAME TYPE

p.describe() #TELLS SOME INFORMATION ABOUT COLUMNS

#TO GET SOME COLUMNS
p[["column_name_1", "column_name_2 "]]

#TO CREATE NEW COLUMN
p["blah"] = p["petal length (cm)"] + 1

#TO GET A COLUMN (IF NO SPACE IN NAME)
P.blah

#TO DROP A COLUMN
p.drop("blah", 1, inplace=True) # p.drop("blah", 0, inplace=True) ---> TO DELETE A ROW

#TO DROP MULTIPLE COLUMNS
p.drop(["blah", "blah1"], 1, inplace=True)

#TO FILL NA
p.fillna(-10, inplace=True)

#TO REPLACE ENTRY IN DATAFRAME
p.replace(4.9, 4, inplace=True)

#TO DROP A ROW WHICH HAS ATLEAST ONE NA VALUE
p.drop()

# IT WORKS ON COLUMN FIRST THEN ROW
#p[COLUMN][ROW]

#SHIFTING
#MAKE NEW p
p["blah"] = 1
p["blah"] = p["blah"].shift(-10) #p["blah"].shift(-10) ---> SHIFTS THE COLUMN ENTRIES UP BY 10 ENTRIES AND SET LAST 10 ENTRIES TO NaN


#CONVERTING STRINGS TO INTEGERS
from sklearn import preprocessing
con = preprocessing.LabelEncoder()
p["blah"] = con.fit_transform(p["blah"])

#SETTING FEATURE IMPORTANCE FROM GIVEN DATAFRAME
from sklearn.feature_selection import RFE
from sklearn.svm import SVR

model = SVR(kernel="linear")

#FEATURE SELECTOR || WE SHOULD DO SCALING BEFORE IMPLEMENTING SELECTOR
selector = RFE(model, 2) #NUMBER OF FEATURES 2 || selector = RFE(model, 2, step=2) ---> remove last 2 features <--- STEP
d = datasets.load_iris()
selector.fit(d.data, d.target)
selector.support_ #OUTPUTS THE SELECTED FEATURES "True" ... Rest as "False" | train(5) - select1, train(4) - select1 , ... , train(2) - select1 ...

# ------
selector.support_[0] = True
selector.support_  #ALWAYS GIVE [0] AS TRUE
# ------

#GIVES RANKING OF FEATURES
selector.ranking_
