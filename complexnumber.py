class ComplexNumber:

    def __init__(self,real=0, img=0):
        self.real = real
        self.img = img

    def __str__(self):
        return("{0}{1}{2}".format(self.real, " + j", self.img))
        

    def setNumber(self):
        self.real = input("Write the real part : ")
        self.img = input("Write the imaginary part : ")

    def add(self,adder):
        return ComplexNumber(self.real+adder.real, self.img+adder.img)

    def subtract(self,sub):
        return ComplexNumber(self.real-sub.real, self.img-sub.img)

    def multiply(self,mul):
        return ComplexNumber((self.real*mul.real)-(self.img*mul.img), (self.real*mul.img)+(self.img*mul.real))


if __name__ == "__main__":
    a = ComplexNumber()
    a.setNumber()
    b = ComplexNumber(1,2)
    c = a.add(b)
    print("ADD a,b")
    print(c)
    d = c.subtract(a)
    print("SUBTRACT c,a")
    print(d)
    print("MULTIPLY a,b")
    e = a.multiply(b)
    print(e)

