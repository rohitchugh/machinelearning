import numpy as np
from sklearn import preprocessing, cross_validation, metrics, svm, datasets, tree, neighbors 
from sklearn.linear_model import LinearRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from matplotlib import style


d = datasets.load_breast_cancer()

X = d.data
Y = d.target

X = preprocessing.scale(X)

X_Train, X_Test, Y_Train, Y_Test =  cross_validation.train_test_split(X, Y, test_size = 0.2)

model = LinearRegression()
model.fit(X_Train, Y_Train)

accu = model.score(X_Test, Y_Test)
predicted = model.predict(X_Test)
print(Y_Test)
predicted = np.round(predicted)
print(predicted)

print(metrics.classification_report(Y_Test, predicted))
print(accu)

svmLinear = svm.SVR(kernel = 'linear')
svmRbf = svm.SVR(kernel = 'rbf')
DecisionTreeClf = tree.DecisionTreeClassifier()
KNN = neighbors.KNeighborsClassifier()
naiveBayes = GaussianNB()

for clf, name in [(svmLinear, 'Linear SVM'), (svmRbf, 'RBF SVM'), (naiveBayes, 'Naive Bayes'), (KNN, 'K Nearest Neighbors'), (DecisionTreeClf,'Descision Tree')]:
    clf.fit(X_Train, Y_Train)
    print(name, clf.score(X_Test, Y_Test))
