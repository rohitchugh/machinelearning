import numpy as np
from sklearn.cluster import KMeans

X = np.array([[1,2], [1.5,1.8], [5,8], [8,8], [1,0.6], [9,11]])

clf = KMeans(n_clusters=2)
clf.fit(X)

clf.cluster_centers_ #SHOWS THE CENTER OF CLUSTERS
clf.labels_ #SHOWS THE CLUSTER TO WHICH POINTS GO

import matplotlib.pyplot as plt
from matplotlib import style

style.use('ggplot')

l = clf.labels_
c = ['r', 'g'] #NUMBER OF COLORS IS EQUAL TO THE NUMBER OF CLUSTERS

for i in range(len(l)):
	plt.scatter([X[i][0]], [X[i][1], color=c[l[i]])
	
plt.show()
#-------------------------------------------------------------------#
import pandas as pd

df = pd.read_csv('autos.csv', ecoding = 'cp1252')
df.info()

df.isnull().sum()
df['notRepairedDamage'].fillna(value='not-declared', inplace=True)
df.dropna(inplace=True)
df.isnull().sum()
df.describe()
df = df.drop(['nrOfPictures'], 1)
df.columns
len(df.name.unique())
df = df.drop(['name'], 1)
df.columns
df.seller.unique()
df.groupby('seller').size()
df = df[(df['seller'] != 'gewerblich')]
df = df.drop(['seller'], 1)
df.columns
from dateutil import parser
ageCol = []
for date in df.dateCreated:
	temp = parser.parse(date)
	ageCol.append(temp.year)
 
df['ageOfVehicle'] = ageCol = df.yearOfRegistration
df = [(df['ageOfVehicle'] > 1) and (df['ageOfVehicle'] < 50)]

from sklearn import preprocessing
Y = df['price']

for col in df.columns:
	df[col] = preprocessing.LabelEncoder().fit_transform(df[col])
	

X = df.drop(df['price'], 1)

X = preprocessing.scale(X)

from sklearn.linear_model import LinearRegression
from sklearn import cross_validation

linearReg = LinearRegression()
x_train, x_test, y_train, y_test = cross_validation.train_test_split(X, Y, test_size = 0.2)
linearReg.fit(x_train, y_train)
print(linearReg.score(x_test, y_test))

from sklearn.ensemble import RandomForestRegressor
import time


rf = RandomForestRegressor()
start = time.time()
rf.fit(x_train, y_train)
print(time.time() - start)
print(rf.score(x_test, y_test))

rf.feature_importances_ #SHOWS THE IMPORTANCE OF FEATURES ... WE CAN DROP WHICH ARE VERY LESS IMPORTANT


#USING PICKLE LIBRARY TO SAVE DATA
with open('linearreg.pickle', 'wb') as f:
	pickle.dump(clf, f)

#READING FROM PICKLE FILE
pickle_in = open('linearreg.pickle', 'rb')
clf = pickle.load(pickle_in)


#KAGGLE --- WEBSITE ... GO TO DATASETS ... QUERY='SIMPLE' ... OTHER PEOPLE CODE IN KERNEL 
