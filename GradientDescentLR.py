import numpy as np
from sklearn import datasets, preprocessing, cross_validation
import matplotlib.pyplot as plt


dataset = datasets.load_breast_cancer()
X = dataset.data
Y = dataset.target

#Scaling data
#X = preprocessing.scale(X)

#Separate training and testing data
#X_Train, X_Test, Y_Train, Y_Test = cross_validation.train_test_split(X, Y, test_size = 0.005)

def gradientDescent(X, Y, theta, alpha, m, num_iterations):
    xTrans = X.transpose()
    for i in range(0, num_iterations):
        hypothesis = np.dot(X, theta)
        loss = hypothesis - Y
        cost = np.sum(loss**2)/(2*m)
        print("Iteration %d | Cost: %f" % (i, cost))
        gradient = np.dot(xTrans, loss)/m
        theta = theta - alpha*gradient
        return theta

num_iterations = 10000
alpha = 0.003
len_of_dataset = len(dataset.data)
num_of_features = len(dataset.feature_names)
m = len_of_dataset
theta = np.ones(num_of_features)
theta = gradientDescent(X, Y, theta, alpha, m, num_iterations)
print(theta)
