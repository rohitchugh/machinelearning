import pandas as pd
from sklearn.feature_selection import RFE
from sklearn.svm import SVC
from sklearn.linear_model import LinearRegression
from sklearn.naive_bayes import GaussianNB
from sklearn import tree, preprocessing, cross_validation, neighbors
import numpy as np
import matplotlib.pyplot as plt
d = pd.read_csv('mushrooms.csv')

con = preprocessing.LabelEncoder()
for i in list(d):
    d[i] = con.fit_transform(d[i])

#data = d.drop(["Type"], 1)
data = d.drop(["class"], 1)
target = d["class"]

LR = LinearRegression()
svmLinear = SVC(kernel = 'linear')
DecisionTreeClf = tree.DecisionTreeClassifier()
KNN = neighbors.KNeighborsClassifier(n_neighbors=8)

print("\nLINEAR REGRESSION")
selectorLR = RFE(LR, 1)
selectorLR.fit(data, target)
print(selectorLR.ranking_)

print("\nSVM LINEAR")
selectorSvmLinear = RFE(svmLinear, 1)
selectorSvmLinear.fit(data, target)
print(selectorSvmLinear.ranking_)


print("\nDT")
selectorDT = RFE(DecisionTreeClf, 1)
selectorDT.fit(data, target)
print(selectorDT.ranking_)

x = preprocessing.scale(data)
y = target

x_train, x_test, y_train, y_test = cross_validation.train_test_split(x,y ,test_size = 0.2)



for clf, name in [(LR, 'Linear Regression'), (svmLinear, 'SVC'),  (DecisionTreeClf, 'Decision Tree'), (KNN, 'KNN')]:
    clf.fit(x_train, y_train)
    print(name, clf.score(x_test, y_test), '\n')

