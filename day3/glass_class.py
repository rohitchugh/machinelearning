import pandas as pd
from sklearn.feature_selection import RFE
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression
from sklearn.naive_bayes import GaussianNB
from sklearn import tree, preprocessing, cross_validation, neighbors
d = pd.read_csv('glass.csv')

#data = d.drop(["Type"], 1)
data = d.drop(["Type", "Si"], 1)
target = d["Type"]

LR = LinearRegression()
svmLinear = SVR(kernel = 'linear')
#svmRbf = SVR(kernel = 'rbf')
DecisionTreeClf = tree.DecisionTreeClassifier()
#naiveBayes = GaussianNB()
KNN = neighbors.KNeighborsClassifier()

print("\nLINEAR REGRESSION")
selectorLR = RFE(LR, 1)
selectorLR.fit(data, target)
print(selectorLR.ranking_)

print("\nSVM LINEAR")
selectorSvmLinear = RFE(svmLinear, 1)
selectorSvmLinear.fit(data, target)
print(selectorSvmLinear.ranking_)


#print("SVM RBF")
#selectorSvmRbf = RFE(svmRbf, 1)
#selectorSvmRbf.fit(data, target)
#print(selectorRbf.ranking_)

print("\nDT")
selectorDT = RFE(DecisionTreeClf, 1)
selectorDT.fit(data, target)
print(selectorDT.ranking_)

#print("NB")
#selectorNB = RFE(naiveBayes, 1)
#selectorNB.fit(data, target)
#print(selectorNB.ranking_)

#print("\nKNN")
#selectorKNN = RFE(KNN, 1)
#selectorKNN.fit(data, target)
#print(selectorKNN.ranking_)

x = preprocessing.scale(data)
#x = data
y = target

x_train, x_test, y_train, y_test = cross_validation.train_test_split(x,y ,test_size = 0.2)



for clf, name in [(LR, 'Linear Regression'), (svmLinear, 'Svm Linear'), (DecisionTreeClf, 'Decision Tree'), (KNN, 'KNN')]:
    clf.fit(x_train, y_train)
    print(name, clf.score(x_test, y_test), '\n')


'''    
LR.fit(x_train, y_train)
accu = LR.score(x_test, y_test)
print("LR ACCU --> ", accu)

svmLinear.fit(x_train, y_train)
accu = svmLinear.score(x_test, y_test)
print("SVM Linear ACCU ---> ", accu)


DecisionTreeClf.fit(x_train, y_train)
accu = DecisionTreeClf.score(x_test, y_test)
print("DT ACCU ---> ", accu)


KNN.fit(x_train, y_train)
accu = KNN.score(x_test, y_test)
print("KNN ACCU ---> ", accu)
'''
