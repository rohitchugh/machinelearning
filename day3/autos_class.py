import pandas as pd
from sklearn.feature_selection import RFE
from sklearn.svm import SVC
from sklearn.linear_model import LinearRegression
from sklearn.naive_bayes import GaussianNB
from sklearn import tree, neighbors, preprocessing, cross_validation
import numpy as np
import matplotlib.pyplot as plt

d = pd.read_csv('autos.csv', encoding = 'cp1252')
d = d.drop(["dateCrawled", "lastSeen", "nrOfPictures",], 1)
print(d.info(), '\n')
print(d.describe(), '\n')
for i in list(d):
    print(i)

