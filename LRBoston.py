import numpy as np
from sklearn import datasets, preprocessing, cross_validation
from sklearn.linear_model import LinearRegression

dataset = datasets.load_boston()
X = dataset.data
Y = dataset.target
# scaling data
print(X[0:2])
X = preprocessing.scale(X)
print(X[0:2])

# separate training and testing data
X_Train, X_Test, Y_Train, Y_Test = cross_validation.train_test_split(X, Y, test_size = 0.02)

model = LinearRegression()
model.fit(X_Train, Y_Train)
b = model.predict(X_Test)
score = model.score(X_Test, Y_Test)
print("FEATURE NAMES ---> ", dataset.feature_names)
print("MODEL COEFFICIENTS ---> ", model.coef_)
print("SCORE ---> ", score)
print("Predicted Values ---> ", b)
print("True Values ---> ", Y_Test)
